﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test_upteka.Models;
namespace Test_upteka.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        string comandString = "";
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddProduct()
        {
            return PartialView(new Product());
        }

        [HttpPost]
        public ActionResult AddProduct(Product newProduct)
        {
            Db.AddProduct(newProduct);
            return RedirectToAction("Index");
        }

        public ActionResult GetProducts()
        {
            comandString = "SELECT ID, NamePr, BarcodePr, SumPr FROM products";
            return PartialView(Db.GetProducts(comandString));
        }

        public ActionResult DeleteProduct(int id)
        {
            Db.DeleteProduct(id);
            return PartialView();
        }

        public ActionResult FormProduct(Product product)
        {
            return PartialView(product);
        }

        public ActionResult EditProduct(int id)
        {
            comandString = "SELECT ID, NamePr, BarcodePr, SumPr FROM products WHERE (ID=" + id + ")";
            return View(Db.GetProducts(comandString).FirstOrDefault());
        }

        [HttpPost]
        public ActionResult EditProduct(Product newProduct)
        {
            Db.EditProduct(newProduct);
            return RedirectToAction("Index");
        }
    }
}
