﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using Test_upteka.Models;

namespace Test_upteka.Models
{
    public class Db
    {
        public static string connectString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\pasha\testing\Test20A\Test_upteka\Test_upteka\App_Data\products.mdb;Persist Security Info=True";
        public static OleDbConnection OleConnection = new OleDbConnection(connectString);

        public static List<Product> GetProducts(string comandString)
        {
            List<Product> ListProduct = new List<Product>();
            try
            {
                OleConnection.Open();
                OleDbCommand cmd = new OleDbCommand(comandString, OleConnection);
                OleDbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListProduct.Add(new Product(reader));
                }
                reader.Close();
                OleConnection.Close();
            }
            catch (Exception) { }
            return ListProduct;
        }

        public static void AddProduct(Product newProduct)
        {
            try
            {
                OleConnection.Open();
                string comandString = "INSERT INTO products  (NamePr, BarcodePr, SumPr) VALUES ('" + newProduct.Name.Trim() + "','" + newProduct.Barcode + "','" + newProduct.Sum + "')";
                OleDbCommand cmd = new OleDbCommand(comandString, OleConnection);
                cmd.ExecuteNonQuery();
                OleConnection.Close();
            }
            catch (Exception) { }
        }

        public static void DeleteProduct(int id)
        {
            try
            {
                OleConnection.Open();
                string comandString = "DELETE FROM products WHERE (ID=" + id + ")";
                OleDbCommand cmd = new OleDbCommand(comandString, OleConnection);
                cmd.ExecuteNonQuery();
                OleConnection.Close();
            }
            catch (Exception) { }
        }

        public static void EditProduct(Product product)
        {
            try
            {
                OleConnection.Open();
                string comandString = "UPDATE products SET NamePr ='" + product.Name + "', BarcodePr=" + product.Barcode + ", SumPr=" + product.Sum + "  WHERE ID=" + product.ID;
                OleDbCommand cmd = new OleDbCommand(comandString, OleConnection);
                cmd.ExecuteNonQuery();
                OleConnection.Close();
            }
            catch (Exception) { }
        }
    }
}