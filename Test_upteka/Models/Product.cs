﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.OleDb;
namespace Test_upteka.Models
{
    public class Product
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "*")]
        [Display(Name = "Наименование")]
        public string Name { get; set; }
        [Required(ErrorMessage = "*")]
        [Display(Name = "Штрих-код")]
        public int Barcode { get; set; }
        [Required(ErrorMessage = "*")]
        [Display(Name = "Колличество")]
        public int Sum { get; set; }

        public Product() { }

        public Product(OleDbDataReader reader)
        {
            ID = Int32.Parse(reader["ID"].ToString());
            Name = reader["NamePr"].ToString();
            Barcode = Int32.Parse(reader["BarcodePr"].ToString());
            Sum = Int32.Parse(reader["SumPr"].ToString());
        }
    }

}